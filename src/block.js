//  Import CSS
import './style.scss';
import './editor.scss';
import './tabs.js';
import classnames from 'classnames';
import times from 'lodash/times';

import FdnTabs from './tabs';

const { __ } = wp.i18n;
const {
	registerBlockType,
	RichText,
	PlainText,
	BlockControls,
	BlockAlignmentToolbar,
	InspectorControls,
} = wp.blocks;

const {
	PanelBody,
} = wp.components;

const { RangeControl, ToggleControl } = InspectorControls;

registerBlockType('tnc/fdn-tabs', {
	title: __('Foundation Tabs'),
	category: 'common',
	icon: 'shield',
	keywords: [
		__('Foundation Tabs')
	],
	attributes: {
		tabsCount: {
			type: 'number',
			default: 2,
		},
		tabs: {
			type: 'array',
			source: 'query',
			selector: '.tabs-wrapper',
			query: {
				title: {
					source: 'children',
					selector: '.tabs-title'
				},
				content: {
					source: 'children',
					selector: '.tabs-panel'
				},
			},
			default: [ {}, {} ],
		},
	},

	edit: FdnTabs,
	save: props => {

		const { className, attributes, id } = props;
		const { tabsCount, tabs } = attributes;

		return [
			<div className="tabs-wrapper">
				<ul class="tabs" data-tabs id={`tabs-${ id }`}>
					{ times( tabsCount, ( index ) =>
					<li
						className={classnames(
							"tabs-title",
							{ 'is-active': index === 0 }
						)}
					>
						<a href={`#${ id }-panel-${ index }`} data-tabs-target={ `${ id }-panel-${ index }` }>
							<span>{ tabs && tabs[ index ].title }</span>
						</a>
					</li>
					) }
				</ul>,
				<div class="tabs-content" data-tabs-content={`tabs-${ id }`}>
					{ times( tabsCount, ( index ) =>
						<div className="tabs-panel" id={ `${ id }-panel-${ index }` }>
							{ tabs && tabs[ index ].content }
						</div>
					) }
				</div>
			</div>
		];
	},
});
