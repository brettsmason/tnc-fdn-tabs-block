import times from 'lodash/times';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { RichText, InspectorControls } = wp.blocks;
const { Component } = wp.element;
const { RangeControl } = InspectorControls;

class FdnTabs extends Component {

	constructor() {
		super( ...arguments );

		this.setTabsAttributes = this.setTabsAttributes.bind( this );
	}

	setTabsAttributes( index, dataObject ) {
		const { attributes } = this.props;
		let existingData = attributes.tabs.slice( 0 ) || [];

		if ( existingData[ index ] ) {
			existingData[ index ] = _.extend( existingData[ index ], dataObject );
		} else {
			existingData[ index ] = dataObject;
		}

		this.props.setAttributes( {
			tabs: existingData
		} );
	}

	componentDidUpdate() {
		const tabs = new Foundation.Tabs(jQuery('.tabs'), {});
	}

	render() {
		const { attributes, setAttributes, isSelected, id } = this.props;
		const { tabsCount, tabs } = attributes;

		console.log(this.props);

		return [
			!! isSelected && (
				<InspectorControls>
					<RangeControl
						label={ __( 'Number of Tabs' ) }
						value={ tabsCount }
						onChange={ ( value ) => setAttributes( { tabsCount: value } ) }
						min={ 2 }
						max={ 10 }
					/>
				</InspectorControls>
			),
			<div className="tabs-wrapper">
				<ul class="tabs" data-tabs id={`tabs-${ id }`}>
					{ times( tabsCount, ( index ) =>
						<li
							className={classnames(
								"tabs-title",
								{ 'is-active': index === 0 }
							)}
							key={ `tab-title-${ index }` }
						>
							<a
								data-tabs-target={ `${ id }-panel-${ index }` }
							>
								<RichText
									tagName="span"
									value={ tabs && tabs[ index ] && tabs[ index ].title }
									onChange={ ( title ) => this.setTabsAttributes( index, { title } ) }
									placeholder={ __( 'Tab Title...' ) }
								/>
							</a>
						</li>
					) }
				</ul>
				<div class="tabs-content" data-tabs-content={`tabs-${ id }`}>
					{ times( tabsCount, ( index ) =>
						<div
							className={classnames(
								"tabs-panel",
								{ 'is-active': index === 0 }
							)}
							id={ `${ id }-panel-${ index }` }
						>
							<RichText
								tagName="div"
								multiline="p"
								value={ tabs && tabs[ index ] && tabs[ index ].content }
								onChange={ ( content ) => this.setTabsAttributes( index, { content } ) }
								placeholder={ __( 'Tab Content...' ) }
							/>
						</div>
					) }
				</div>
			</div>
		];
	}
}

export default FdnTabs;
