<?php
/**
 * Plugin Name: the naked creative: Foundation Tabs Block
 * Plugin URI: http://thenakedcreative.co.uk
 * Description: A simple Foundation tabs block.
 * Author: Brett Mason
 * Version: 1.0.0
 * License: GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

defined( 'ABSPATH' ) || exit;

require_once plugin_dir_path( __FILE__ ) . 'src/block.php';
